﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using APIQuotation.Models;
using System.IO;
using Wkhtmltopdf.NetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;

namespace APIQuotation.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly QuotationContext _context;
        private readonly IPdfService _ipdfService;
        public IConfiguration Configuration { get; }


        public FileController(QuotationContext context, IPdfService ipdfService, IConfiguration configuration)
        {
            _context = context;
            _ipdfService = ipdfService;
            Configuration = configuration;
        }
        // GET: api/Customers
        [HttpGet]
        public IActionResult GetPdfs()
        {

            bool x = "202108_01".Contains("2021");
            return Ok(new { data = String.IsNullOrEmpty("") , data2 = x , data3= String.IsNullOrEmpty("hihi") });
        }
            [HttpGet("{id}")]
        public async Task<ActionResult<IEnumerable<Customer>>> GetPdf(string id, [FromQuery] Verify verify)
        {
            var qs = _context.Set<Quotation>().ToDictionary(e => e.Quotation_ID)[id];

            string[] x = qs.Customer_ID.Split("_");

            var qss = _context.Set<Customer_Employee>().ToDictionary(e => e.ID)[qs.Customer_ID];

            var qssss = _context.Set<Customer_Company>().ToDictionary(e => e.Company_ID)[qss.Company_ID];

            var qsss = _context.Set<Quotation_Products>().ToDictionary(e => e.ID).Where(e => e.Value.Quotation_ID == id);

            var qsssss = _context.Set<Payment>().ToDictionary(e => e.Payment_ID)[id];


            string[] subs = qs.Remark.Split('\n');

 


            var getpdf = _ipdfService.GeneratePdfReport(new Data_Print()
            {
                Quotation = new Quotation_Print() { Project_Name = qs.Project_Name, Quotation_ID = qs.Quotation_ID, Date = qs.Date.ToString("yyyyMMdd"), Quotation_Deadline = qs.Quotation_Deadline.ToString("yyyyMMdd"), total = qs.Total, tax = qs.Total * 0.05m, total_withTax = qs.Total_withTax, Customer_ID = qs.Customer_ID, Remark = subs },
                Customer = new Customer_Print() { Customer_ID = qss.Company_ID, Customer_Name = qss.Customer_Name, Company_Name = qssss.Company_Name, Company_Phone = qss.Customer_Phone, Company_Fax = qssss.Company_Fax },
                Products = from m in qsss
                           join ms in _context.Set<Product>()
                           on m.Value.Product_ID equals ms.Product_ID
                           select new Products_Print() { Product_ID = m.Value.Product_ID, Product_Name = ms.Product_Name, Amount = m.Value.Amount, Product_Detail = m.Value.Product_Detail, Price = m.Value.Price, Discount = m.Value.Discount, Subtotal = m.Value.Subtotal },
                Payment = new Payment_Print() { Payment_Method = qsssss.Payment_Method }
            });

            return File(getpdf, "application/pdf",id+".pdf");
        }
        [HttpPost("{id}")]
        public async Task<ActionResult<Quotation>> PostFile(string id,[FromForm] IFormFile file)
        {
            if (file.Length > 0)
            {
                string vsfilename = file.FileName;//獲取檔案的名稱
                int index = vsfilename.LastIndexOf(".");
                string vstype = vsfilename.Substring(index).ToLower();

                if(vstype == ".jpg"||vstype == ".pdf"||vstype == ".jpeg" || vstype == ".png")
                {



                    var path = Configuration.GetValue<string>("SaveFileSettings:Path") + $@"/UploadFolder/{id + vstype}";

                    using (var stream = System.IO.File.Create(path))
            {
                await file.CopyToAsync(stream);
            }
                    var getdate = "-"+DateTime.Now.ToString("yyyyMMdd_hhmmss");

                    var path2 = Configuration.GetValue<string>("SaveFileSettings:Path") + $@"\BackupFolder\{id+getdate + vstype}";

                using (var stream = System.IO.File.Create(path2))
                {
                    await file.CopyToAsync(stream);
                }
                var q=_context.Quotations.Find(id);
                q.IsSignback = true;
                q.Project_Signback = path;
                _context.Entry(q).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                return Ok(new { Status_Code = 2000, filePath = path});
                }

                return Ok(new { Status_Code = 4105, Message = "錯誤檔案格式",type= vstype });
            }
            return Ok(new { Status_Code = 4000});
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Quotation>> DeleteFile(string id)
        {
       
            var q = _context.Quotations.Find(id);

            if (System.IO.File.Exists(q.Project_Signback))
            {
                System.IO.File.Delete(q.Project_Signback);
            }
            
            q.IsSignback = false;

            q.Project_Signback = "";
            _context.Entry(q).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return Ok(new { Status_Code = 2000});
        }
    }
}
