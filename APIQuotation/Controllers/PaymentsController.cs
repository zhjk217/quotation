﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using APIQuotation.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIQuotation.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private readonly QuotationContext _context;

        public PaymentController(QuotationContext context)
        {
            _context = context;

        }

        // GET: api/Payments/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Payment>> GetPayment(string id)
        {
            var query = _context.Quotations.Find(id);

            var querys = _context.Payments.Find(id);

            int Status =0;

            switch (query.Status)
            // switch 報價單狀態 分類
            {
                case "待追蹤":
                    //待追蹤
                    Status = 1;
                    break;
                case "進行中":
                    //進行中
                    Status = 2;
                    break;
                case "完成":
                    //完成
                    Status = 3;
                    break;
                case "作廢":
                    //作廢
                    Status = 4;
                    break;
                case "其他":
                    //其他
                    Status = 5;
                    break;
            }

            int Payment_Method = 0;

            switch (querys.Payment_Method)
            // switch 報價單狀態 分類
            {
                case "一次付清":
                    //待追蹤
                    Payment_Method = 1;
                    break;
                case "月結":
                    //進行中
                    Payment_Method = 2;
                    break;
                case "次月結":
                    //完成
                    Payment_Method = 3;
                    break;
                case "次次月結":
                    //作廢
                    Payment_Method = 4;
                    break;
                case "Pay2Pay":
                    //其他
                    Payment_Method = 5;
                    break;
                case "二期":
                    //其他
                    Payment_Method = 6;
                    break;
                case "三期":
                    //其他
                    Payment_Method = 7;
                    break;
                case "四期":
                    //其他
                    Payment_Method = 8;
                    break;
                case "五期":
                    //其他
                    Payment_Method = 9;
                    break;
                default:
                    Payment_Method = 1;
                    break;
            }

            int Payment_Status = 1;

            switch (querys.Payment_Status)
            // switch 報價單狀態 分類
            {
                case "尚未付款":
                    Payment_Status = 1;
                    break;
                case "待催帳-第一期":
                    //待追蹤
                    Payment_Status = 2;
                    break;
                case "待催帳-第二期":
                    //進行中
                    Payment_Status = 3;
                    break;
                case "待催帳-第三期":
                    //完成
                    Payment_Status = 4;
                    break;
                case "待催帳-第四期":
                    //作廢
                    Payment_Status = 5;
                    break;
                case "待催帳-第五期":
                    //其他
                    Payment_Status = 6;
                    break;
                case "已付清":
                    //其他
                    Payment_Status = 7;
                    break;
                default:
                    Payment_Status = 1;
                    break;
            }

            var Success_Message = new { Status_Code = 2000, Data = new { Status, Payment_Status, Payment_Method,query.Project_Remark} };
            return Ok(Success_Message);
        }

        // PUT: api/Payments/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutStatusQuotations(string id, Verify_Quotation_PUT_Status data)
        {
            var query = _context.Quotations.Find(id);

            String Status = "";

            switch (data.Status)
            // switch 報價單狀態 分類
            {
                case 1:
                    //待追蹤
                    Status = "待追蹤";
                    break;
                case 2:
                    //進行中
                    Status = "進行中";
                    break;
                case 3:
                    //完成
                    Status = "完成";
                    break;
                case 4:
                    //作廢
                    Status = "作廢";
                    break;
                case 5:
                    //其他
                    Status = "其他";
                    break;
            }
            //報價單狀態在資料庫儲存的是 String
            // int -> switch分類 -> String

            String Payment_Method = "";

            switch (data.Payment_Method)
            // switch 報價單狀態 分類
            {
                case 1:
                    //待追蹤
                    Payment_Method = "一次付清";
                    break;
                case 2:
                    //進行中
                    Payment_Method = "月結";
                    break;
                case 3:
                    //完成
                    Payment_Method = "次月結";
                    break;
                case 4:
                    //作廢
                    Payment_Method = "次次月結";
                    break;
                case 5:
                    //其他
                    Payment_Method = "Pay2Pay";
                    break;
                case 6:
                    //其他
                    Payment_Method = "二期";
                    break;
                case 7:
                    //其他
                    Payment_Method = "三期";
                    break;
                case 8:
                    //其他
                    Payment_Method = "四期";
                    break;
                case 9:
                    //其他
                    Payment_Method = "五期";
                    break;
            }

            String Payment_Status = "";

            switch (data.Payment_Status)
            // switch 報價單狀態 分類
            {
                case 1:
                    Payment_Status = "尚未付款";
                    break;
                case 2:
                    //待追蹤
                    Payment_Status = "待催帳-第一期";
                    break;
                case 3:
                    //進行中
                    Payment_Status = "待催帳-第二期";
                    break;
                case 4:
                    //完成
                    Payment_Status = "待催帳-第三期";
                    break;
                case 5:
                    //作廢
                    Payment_Status = "待催帳-第四期";
                    break;
                case 6:
                    //其他
                    Payment_Status = "待催帳-第五期";
                    break;
                case 7:
                    //其他
                    Payment_Status = "已付清";
                    break;
            }


            query.Status = Status;

            query.Project_Remark = data.Remark;

            _context.Entry(query).State = EntityState.Modified;

            var query2 = _context.Payments.Find(id);

            query2.Payment_Method = Payment_Method;

            query2.Payment_Status = Payment_Status;

            _context.Entry(query2).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }

    }
}
