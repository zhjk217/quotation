﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIQuotation.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIQuotation.Controllers
{
    [Authorize]
    //api/v1
    [Route("api/v1/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly QuotationContext _context;
        private readonly JwtHelpers jwt;

        public UsersController(QuotationContext context, JwtHelpers jwt)
        {
            _context = context;
            this.jwt = jwt;
        }
        [HttpGet]
        public IActionResult Getaccount()
        //取得帳號資料 (帳號)
        {
            var Account = User.Identity.Name;
            //User使用者透過Token連進來
            //JWT解碼Identity.Name

            var q = _context.Users.Where(e=>e.Account == Account).FirstOrDefault().Name;

            var Success_Message = new { Status_Code = 2000, Account = q };
            return Ok(Success_Message);
        }
        [Route("Company")]
        [HttpGet]
        public IActionResult Company_IDIsExists([FromQuery] string Company_ID)
        //公司編號是否存在
        {
            bool Exist = Company_Exists(Company_ID);
            //LINQ的Any
            //bool判斷公司編號是否存在


            if (Exist)
            //若存在回傳成功
            {
                var Success_Message = new { Status_Code = 2000, Exist = Exist };
                return Ok(Success_Message);
            }
            //若不存在回傳失敗
            var Wrong_Message = new { Status_Code = 4401, Exist = Exist };
            return Ok(Wrong_Message);
        }
        [Route("Signout")]
        [HttpGet]
        public IActionResult Signout()
        //登出
        {
            // jwt.RemoveRefreshTokenByUserName(userName);
            var Success_Message = new { Status_Code = 2000,name = "account" };
            return Ok(Success_Message);
        }
        [Route("Signup")]
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Signup(User_Signup user)
        //註冊
        {
            if(String.IsNullOrEmpty(user.Account)|| String.IsNullOrEmpty(user.Password)|| String.IsNullOrEmpty(user.Company_ID))
            //判斷值是否為 "" 或 null
            //若是回傳錯誤
            {
                var Wrong_Message = new { Status_Code = 4102, Message = "使用者格式錯誤" };
                return Ok(Wrong_Message);
            }

            if (!Company_Exists(user.Company_ID))
            //LINQ的Any
            //bool判斷帳號是否存在
            //若不存在增加此公司
            {
                _context.Office.Add(new Office() { Company_ID = user.Company_ID });
                //公司資料 Add  
            }


            if (User_Account_Exists(user.Account))
            //LINQ的Any
            //bool判斷帳號是否存在
            //若存在回傳失敗
            {
                var Wrong_Message = new { Status_Code = 4402, Message = "使用者已存在" };
                return Ok(Wrong_Message);
            }

            User User = new User() { Account = user.Account, Password = user.Password, Company_ID = user.Company_ID,Name=user.Name,Email=user.Email,Phone=user.Phone};
            //重構 new User

            _context.Users.Add(User);
            //使用者 Add 


             await _context.SaveChangesAsync();
            //Save 資料庫

            var Success_Message = new { Status_Code = 2000 , User_ID = User.User_ID,token= jwt.GenerateToken(user.Account) };
            return Ok(Success_Message);
        }
        [Route("Signin")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Signin(User_Signin user)
        //登入
        {
            if (String.IsNullOrEmpty(user.Account) || String.IsNullOrEmpty(user.Password))
            //判斷值是否為 "" 或 null
            //若是回傳錯誤
            {
                var Wrong_Message = new { Status_Code = 4102, Message = "使用者格式錯誤" };
                return Ok(Wrong_Message);
            }

            var User =_context.Users.FirstOrDefault(e=>e.Account==user.Account);
            // 用 acount 做 FirstOrDefault 有或null
            // 拿取 User 唯一值


            if (User != null)
            // null 排除
            {

                if (User.Password == user.Password)
                // 對比密碼
                //若正確回傳成功
                {
                    var Success_Message = new { Status_Code = 2000, User_ID = User.User_ID, token = jwt.GenerateToken(User.Account) };
                    return Ok(Success_Message);
                }
                //若不正確回傳失敗
            }
            //若null回傳失敗
            var Wrong_Message2 = new { Status_Code = 4403, Message = "帳號或密碼錯誤" };
            return Ok(Wrong_Message2);
        }
        [Route("Personal")]
        [HttpGet]
        public IActionResult GetUsers()
        //取得帳號資料 (帳號)
        {
         
            var q = _context.Users.Where(e=>e.Account == User.Identity.Name).FirstOrDefault();

            if (q == null)
            {

                var M = new { Status_Code = 2000};
                return Ok(M);
            }

            var Success_Message = new { Status_Code = 2000 , data=new {q.Name,q.Phone,q.Email } };
            return Ok(Success_Message);
        }
        [Route("Personal")]
        [HttpPut]
        public IActionResult PutUsers(User user)
        //取得帳號資料 (帳號)
        {
            var q = _context.Users.Where(e => e.Account == User.Identity.Name).FirstOrDefault();

            q.Name = user.Name;

            q.Phone = user.Phone;

            q.Email = user.Email;

            _context.Entry(q).State = EntityState.Modified;

            _context.SaveChanges();

            var Success_Message = new { Status_Code = 2000};
            return Ok(Success_Message);
        }
        [Route("Projects")]
        [HttpGet]
        public IActionResult GetProjects()
        //取得帳號資料 (帳號)
        {
            var q = (from qs in _context.Set<Project>()
      
                    select new
                    {
                        value=qs.ID,
                        text=qs.Project_Name
                    }).AsQueryable().ToList();

            var Success_Message = new { Status_Code = 2000, data = q };
            return Ok(Success_Message);
        }
        [Route("Project/{id}")]
        [HttpGet]
        public IActionResult GetProject(int id)
        //取得帳號資料 (帳號)
        {
            var q = _context.Set<Project>().Find(id);

            var Success_Message = new { Status_Code = 2000, data = new { q.ID , q.Project_Name } };
            return Ok(Success_Message);
        }
        [Route("Project/{id}")]
        [HttpPut]
        public IActionResult PutProject(int id, Project project)
        //取得帳號資料 (帳號)
        {

            var q = _context.Set<Project>().Find(id);

            q.Project_Name = project.Project_Name;

            _context.Entry(q).State = EntityState.Modified;

            _context.SaveChanges();

            var Success_Message = new { Status_Code = 2000};
            return Ok(Success_Message);
        }
        [Route("Project/{id}")]
        [HttpDelete]
        public IActionResult DeleteProject(int id)
        //取得帳號資料 (帳號)
        {

            var q = _context.Set<Project>().Find(id);

            q.IsDelete = true;

            _context.Entry(q).State = EntityState.Modified;

            _context.SaveChanges();

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        private bool User_Account_Exists(string account)
        //使用者帳號是否存在
        {
            return _context.Users.Any(e => e.Account == account);
        }
        private bool Company_Exists(string id)
        //公司編號是否存在
        {
            return _context.Office.Any(e => e.Company_ID == id);
        }
    }
}
