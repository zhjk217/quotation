﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIQuotation.Models;
using System.Text.Json;
using Newtonsoft.Json;
using Wkhtmltopdf.NetCore;
using Microsoft.AspNetCore.Authorization;

namespace APIQuotation.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QuotationsController : ControllerBase
    {
        private readonly QuotationContext _context;

        public QuotationsController(QuotationContext context)
        {
            _context = context;
        }
        //async Task<ActionResult> 
        //IActionResult
        [HttpGet]
        public async Task<ActionResult> Get_AllQuotations([FromQuery] Quotation_p data)
        // 主頁面 - 列出所有報價單
        {


            List<year_front> s = new List<year_front>();
            s.Add(new year_front() { text = "顯示全部", value = 1 });
            for (int i = 1; i < 100; i++)
            {


                int date = int.Parse(DateTime.Now.ToString("yyyy")) - i+1;
                
                bool b =_context.Quotations.Any(e=>e.Quotation_ID.Contains(date.ToString()));

                if (!b)
                {
                    break;
                }
                s.Add(new year_front() { text = date.ToString(), value = i+1 });
            }

            if (data.Year == 0)
            //頁面判斷
            //頁面為空則填上 Page = 1;
            {
                data.Year = 2;

            }
            if (data.Month == 0)
            //頁面判斷
            //頁面為空則填上 Page = 1;
            {
                data.Month = int.Parse(DateTime.Now.ToString("MM")) + 1;
            }
            if (data.Signback == 0)
            //頁面判斷
            //頁面為空則填上 Page = 1;
            {
                data.Signback = 1;
            }

            var Success_Message = new { Status_Code = 2000,time=s ,month= int.Parse(DateTime.Now.ToString("MM")), Data = await List_AllQuotations(data, _context.Set<Quotation>(),s).ToListAsync() };
            return Ok(Success_Message);
        }

        [NonAction]
        private IQueryable<Quotation_List> List_AllQuotations(Quotation_p data, IQueryable<Quotation> Quotations, List<year_front> s)
        {

            String Year = "";

            if (data.Year == 1)
            {
                Year = "";
            }
            else
            {
                Year = s[data.Year - 1].text;
            }

            String Month = "";


            if (data.Month == 1)
            {
                Month = "";
            }
            else
            {
                if (data.Month <= 10)
                {
                    Month = "0" + (data.Month - 1).ToString();
                }

                Month = (data.Month - 1).ToString();
            }

           var Customer_List = _context.Set<Customer_Company>().ToDictionary(e => e.Company_ID);

            var  Quotations_List = from Quotation in Quotations
                                    where !Quotation.IsDelete && Quotation.Status != "作廢" && Quotation.Quotation_ID.Substring(0, 4).Contains(Year) && Quotation.Quotation_ID.Substring(4, 2).Contains(Month)
                                orderby Quotation.Quotation_ID descending
                                 join Customers in _context.Set<Customer_Employee>()
                        on Quotation.Customer_ID equals Customers.ID
                        select new Quotation_List()
                        {
                            Quotation_ID = Quotation.Quotation_ID,
                            Company_Name = Customer_List[Customers.Company_ID].Company_Name,
                            Project_Name = Quotation.Project_Name,
                            IsSignback = Quotation.IsSignback
                        };


            if (!String.IsNullOrEmpty(data.Search))
            {
                Quotations_List = Quotations_List.Where(e => e.Quotation_ID.Contains(data.Search) || e.Project_Name.Contains(data.Search) || e.Company_Name.Contains(data.Search));
            }

            if(data.Signback != 1)
            {
                bool Signback = false;

                switch (data.Signback)
                // switch 回簽檔 分類
                {
                    case 2:
                        Signback = true;
                        //已上傳回簽檔
                        break;
                    case 3:
                        Signback = false;
                        //未上傳回簽檔
                        break;
                }

                Quotations_List = Quotations_List.Where(e => e.IsSignback == Signback);
            }

            return Quotations_List;

        }
        [Route("Status")]
        [HttpGet]
        public async Task<ActionResult> GetAllStatusQuotations([FromQuery] Quotation_Status data)
        // 追蹤狀態 - 列出所有報價單(追蹤)
        {

            if (data.Year == 0)
            //頁面判斷
            //頁面為空則填上 Page = 1;
            {
                data.Year = 2;

            }
            if (data.Month == 0)
            //頁面判斷
            //頁面為空則填上 Page = 1;
            {
                data.Month = int.Parse(DateTime.Now.ToString("MM")) + 1;
            }

            if (data.Status ==0)
            {
                data.Status = 1;
            }
            if(data.Payment_Method == 0)
            {
                data.Payment_Method = 1;
            }
            if(data.Payment_Status == 0)
            {
                data.Payment_Status = 1;
            }

            List<year_front> s = new List<year_front>();
            s.Add(new year_front() { text = "顯示全部", value = 1 });
            for (int i = 1; i < 100; i++)
            {


                int date = int.Parse(DateTime.Now.ToString("yyyy")) - i + 1;

                bool b = _context.Quotations.Any(e => e.Quotation_ID.Contains(date.ToString()));

                if (!b)
                {
                    break;
                }
                s.Add(new year_front() { text = date.ToString(), value = i + 1 });
            }

            var Success_Message = new { Status_Code = 2000,time=s,  month = int.Parse(DateTime.Now.ToString("MM")), Data = await List_AllStatusQuotations(data,_context.Set<Quotation>(),s).ToListAsync() };
            return Ok(Success_Message);
        }
        [NonAction]
        private IQueryable<StatusQuotation_List> List_AllStatusQuotations(Quotation_Status data, IQueryable<Quotation> Quotations, List<year_front> s)
        {
            String Year = "";

            if (data.Year == 1)
            {
                Year = "";
            }
            else
            {
                Year = s[data.Year - 1].text;
            }

            String Month = "";


            if (data.Month == 1)
            {
                Month = "";
            }
            else
            {
                if (data.Month <= 10)
                {
                    Month = "0" + (data.Month - 1).ToString();
                }

                Month = (data.Month - 1).ToString();
            }


            var Quotations_List =(
                from Quotation in Quotations
                where !Quotation.IsDelete && Quotation.Quotation_ID.Substring(0, 4).Contains(Year) && Quotation.Quotation_ID.Substring(4, 2).Contains(Month)
                orderby Quotation.Quotation_ID descending
                join Payments in _context.Set<Payment>()
                on Quotation.Quotation_ID equals Payments.Payment_ID
                select new StatusQuotation_List()
                {
                    Status = Quotation.Status,
                    Quotation_ID = Quotation.Quotation_ID,
                    Project_Name = Quotation.Project_Name,
                    Payment_Status = Payments.Payment_Status,
                    Payment_Method = Payments.Payment_Method,
                    Project_Remark = Quotation.Project_Remark
                }
           );

            if (!String.IsNullOrEmpty(data.Search))
            {
                Quotations_List = Quotations_List.Where(e => e.Quotation_ID.Contains(data.Search) || e.Project_Name.Contains(data.Search) || e.Project_Remark.Contains(data.Search));
            }

            if (data.Status != 1)
            {
                String Status = "";

                switch (data.Status)
                // switch 報價單狀態 分類
                {
                    case 1:
                        //顯示全部
                        Status = "";
                        break;
                    case 2:
                        //待追蹤
                        Status = "待追蹤";
                        break;
                    case 3:
                        //進行中
                        Status = "進行中";
                        break;
                    case 4:
                        //完成
                        Status = "完成";
                        break;
                    case 5:
                        //作廢
                        Status = "作廢";
                        break;
                    case 6:
                        //其他
                        Status = "其他";
                        break;
                    default:
                        Status = "";
                        break;
                }
                //報價單狀態在資料庫儲存的是 String
                // int -> switch分類 -> String

                Quotations_List = Quotations_List.Where(e => e.Status == Status);
            }

            if (data.Payment_Method != 1)
            {



                String Payment_Method = "";

                switch (data.Payment_Method)
                // switch 報價單狀態 分類
                {
                    case 1:
                        //顯示全部
                        Payment_Method = "";
                        break;
                    case 2:
                        //待追蹤
                        Payment_Method = "一次付清";
                        break;
                    case 3:
                        //進行中
                        Payment_Method = "月結";
                        break;
                    case 4:
                        //完成
                        Payment_Method = "次月結";
                        break;
                    case 5:
                        //作廢
                        Payment_Method = "次次月結";
                        break;
                    case 6:
                        //其他
                        Payment_Method = "Pay2Pay";
                        break;
                    case 7:
                        //其他
                        Payment_Method = "二期";
                        break;
                    case 8:
                        //其他
                        Payment_Method = "三期";
                        break;
                    case 9:
                        //其他
                        Payment_Method = "四期";
                        break;
                    case 10:
                        //其他
                        Payment_Method = "五期";
                        break;
                    default:
                        Payment_Method = "";
                        break;
                }

                Quotations_List = Quotations_List.Where(e => e.Payment_Method == Payment_Method);
            }

            if (data.Payment_Status != 1)
            {


                String Payment_Status = "";

                switch (data.Payment_Status)
                // switch 報價單狀態 分類
                {
                    case 1:
                        //顯示全部
                        Payment_Status = "";
                        break;
                    case 2:
                        Payment_Status = "尚未付款";
                        break;
                    case 3:
                        //待追蹤
                        Payment_Status = "待催帳-第一期";
                        break;
                    case 4:
                        //進行中
                        Payment_Status = "待催帳-第二期";
                        break;
                    case 5:
                        //完成
                        Payment_Status = "待催帳-第三期";
                        break;
                    case 6:
                        //作廢
                        Payment_Status = "待催帳-第四期";
                        break;
                    case 7:
                        //其他
                        Payment_Status = "待催帳-第五期";
                        break;
                    case 8:
                        //其他
                        Payment_Status = "已付清";
                        break;
                    default:
                        Payment_Status = "";
                        break;
                }

                Quotations_List = Quotations_List.Where(e=>e.Payment_Status == Payment_Status);
            }
            //  LINQ 搜尋   從   報價單
            //              排除 刪除的 與 作廢的
            //              排序 從 最後(最新) 開始
            //                    加入 付款
            //                    on 報價單.報價單編號 equals 付款.付款編號 (1*)
            //              重構 new StatusQuotation_List
            //                    報價單狀態,報價單編號,專案名稱,付款狀態,報價單備註(2*)
            //          跳過 (Page-1) * 6
            //          擷取 Page * 6

            // 一頁 6 項


            // 1* 報價單編號跟付款編號 一樣

            // 2* 報價單有兩個備註  一個給客戶看的 Quotation.Remark 一個給管理看的 Quotation.Project_Remark
            return Quotations_List;
        }
    }
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class QuotationController : ControllerBase
    {
        private readonly QuotationContext _context;
        private readonly IPdfService _ipdfService;

        public QuotationController(QuotationContext context, IPdfService ipdfService)
        {
            _context = context;
            _ipdfService = ipdfService;
        }
        [HttpPost]
        public async Task<ActionResult> PostQuotation(Data data)
        // 表單頁面 - 送出報價單
        {
            if (String.IsNullOrEmpty(data.Customer.Customer_ID)|| String.IsNullOrEmpty(data.Customer.Customer_Name) || String.IsNullOrEmpty(data.Customer.Company_Name) || String.IsNullOrEmpty(data.Customer.Company_Phone) || String.IsNullOrEmpty(data.Customer.Company_Fax))
            //判斷值是否為 "" 或 null
            //若是回傳錯誤
            {
                return Ok(new { Status_Code= 4102 ,Message="客戶格式錯誤"});
            }
 
            if (String.IsNullOrEmpty(data.Customer.Customer_ID) || String.IsNullOrEmpty(data.Customer.Customer_Name) || String.IsNullOrEmpty(data.Customer.Company_Name) || String.IsNullOrEmpty(data.Customer.Company_Phone) || String.IsNullOrEmpty(data.Customer.Company_Fax))
            //判斷值是否為 "" 或 null
            //若是回傳錯誤
            {
                return Ok(new { Status_Code = 4102, Message = "客戶格式錯誤" });
            }

            if (String.IsNullOrEmpty(data.Quotation.Project_Name) || data.Quotation.Total <= 0|| String.IsNullOrEmpty(data.Quotation.Remark))
            //判斷值是否為 "" 或 null   0 或 負值
            //若是回傳錯誤
            {
                return Ok(new { Status_Code = 4102, Message = "報價單格式錯誤" });
            }

            using var transaction = _context.Database.BeginTransaction();
            //資料庫交易 - 打開交易
            // 進貨單主檔 - 進貨單明細 - 產品庫存
            // .net core to dbContext - dbContext - dbContext to MySQL 
            // .net core to dbContext 
            // 打開交易 BEGIN TRANSACTION
            // 確認交易 COMMIT TRANSACTION
            // 完成交易 ROLLBACK TRANSACTION

            var c =_context.Customer_Companys.Find(data.Customer.Customer_ID);
            if(_context.Customer_Companys.Any(e => e.Company_ID == data.Customer.Customer_ID)){
                if (!( (c != null && c.Company_Name == data.Customer.Company_Name && c.Company_Fax == data.Customer.Company_Fax)))
                {
                    return Ok(new { Status_Code = 4102, Message = "客戶公司格式錯誤" });
                }
            }

            if (!_context.Customer_Companys.Any(e => e.Company_ID == data.Customer.Customer_ID))
            //判斷此客戶是否存在
            //若是新增客戶
            {


                _context.Customer_Companys.Add(new Customer_Company() {Company_ID= data.Customer.Customer_ID ,Company_Name=data.Customer.Company_Name , Company_Fax = data.Customer.Company_Fax});
            }

            var b = _context.Customer_Employees.Where(e => e.Company_ID == data.Customer.Customer_ID);

            var dd = "";

            if (!b.Any(e => e.Customer_Name == data.Customer.Customer_Name))
            //判斷此客戶是否存在
            //若是新增客戶
            {
                dd = data.Customer.Customer_ID + "_" + (b.Count() + 1).ToString();
                _context.Customer_Employees.Add(new Customer_Employee() { ID = dd, Company_ID = data.Customer.Customer_ID, Customer_Name = data.Customer.Customer_Name, Customer_Phone = data.Customer.Company_Phone });
            }
            else
            {
                dd = data.Customer.Customer_ID + "_" + (b.Count()).ToString();
            }

            data.Quotation.Customer_ID = dd;
            //寫入 客戶編號
            data.Quotation.Quotation_ID = GetDate();
            //寫入 報價單編號 GetDate()
            data.Quotation.Date = DateTime.Now;
            //寫入 日期
            data.Quotation.Quotation_Deadline = DateTime.Now.AddDays(+30);
            //寫入 期限 +30天
            data.Quotation.Status = "待追蹤";
            //寫入 狀態
            data.Quotation.IsDelete = false;
            //寫入 Is刪除
            data.Quotation.IsSignback = false;
            //寫入 Is回簽檔
            data.Quotation.Project_Remark = "";
            //寫入 報價單備註 (給管理看的)
            data.Quotation.Project_Owner = User.Identity.Name;
            //寫入 專案負責人
            data.Quotation.Total_withTax = data.Quotation.Total * 1.05m;
            //寫入 總計+稅額

            _context.Quotations.Add(data.Quotation);
            //報價單 Add 

            if (!_context.Projects.Where(e=>e.IsDelete==false).Any(e=>e.Project_Name == data.Quotation.Project_Name))
            {
                _context.Projects.Add(new Project() { Project_Name = data.Quotation.Project_Name });
            }

            if (String.IsNullOrEmpty(data.Payment.Payment_Method))
            {
                data.Payment.Payment_Method = "一次付清";
            }

            _context.Payments.Add(new Payment() { Payment_ID = data.Quotation.Quotation_ID, Total = data.Quotation.Total,Payment_Method= data.Payment.Payment_Method , Payment_Status= "尚未付款" });
            //重構 付款
            //付款 Add

            int x;
            // 設 x 值 為 填入 報價單_產品列表 的 產品編號
            // 若無此值 報價單_產品列表 的 產品為空 則 出錯
            // 先假設 x 為 產品 在之後做 報價單_產品列表 做 填寫時 才不會出錯
            if (_context.Set<Product>().Count() == 0)
            //產品若空的
            {
                x = 0;
                //設0
            }
            else
            {
                x = _context.Set<Product>().OrderBy(e => e.Product_ID).Last().Product_ID;
                //找最後一筆
            }

            for (int i = 0; i < data.Products.Count(); i++)
            //for 去跑 data.產品.長度
            {
                if (String.IsNullOrEmpty(data.Products[i].Product_Name)|| String.IsNullOrEmpty(data.Products[i].Product_Detail) || data.Products[i].Price <= 0 || data.Products[i].Amount <= 0|| data.Products[i].Discount <= 0|| data.Products[i].Subtotal <= 0)
                //判斷值是否為 "" 或 null   0 或 負值
                //若是回傳錯誤
                {
                    return Ok(new { Status_Code = 4102, Message = "產品格式錯誤" });
                }


                if (data.Products[i].Product_ID == 0)
                //判斷產品是否為空
                //若是新增產品
                {
                    _context.Products.Add(new Product() { Product_Name = data.Products[i].Product_Name, Price = data.Products[i].Price,Product_Detail=data.Products[i].Product_Detail });
                    //重構 產品
                    //產品 Add
                    x++;
                    data.Products[i].Product_ID = x;
                    //假設 x 為 產品編號
                }
            }

            await _context.SaveChangesAsync();
            //Save 資料庫

            for (int i = 0; i < data.Products.Count(); i++)
            {
                _context.Quotation_Products.Add(new Quotation_Products() { Quotation_ID = data.Quotation.Quotation_ID, Product_ID = data.Products[i].Product_ID, Amount = data.Products[i].Amount, Price = data.Products[i].Price, Discount = data.Products[i].Discount, Subtotal = data.Products[i].Subtotal, Product_Detail = data.Products[i].Product_Detail });
                //重構 報價單_產品列表
                //報價單_產品列表 Add
            }
            await _context.SaveChangesAsync();
            //Save 資料庫
            transaction.Commit();
            // 確認交易

            /*
            string[] subs = data.Quotation.Remark.Split('\n');
            // 把 備註 String 截斷\n 成為 備註 Array

            var getpdf = _ipdfService.GeneratePdfReport(new Data_Print()
            // PDF 
            {
                Quotation = new Quotation_Print() {Project_Name=data.Quotation.Project_Name,Quotation_ID=data.Quotation.Quotation_ID,Date=data.Quotation.Date.ToString("yyyyMMdd"), Quotation_Deadline=data.Quotation.Quotation_Deadline.ToString("yyyyMMdd"), total=data.Quotation.Total, total_withTax=data.Quotation.Total_withTax, Customer_ID=data.Quotation.Customer_ID,Remark= subs },
                Customer = new Customer_Print() { Customer_ID = data.Customer.Customer_ID, Customer_Name = data.Customer.Customer_Name, Company_Name = data.Customer.Company_Name, Company_Phone = data.Customer.Company_Phone, Company_Fax = data.Customer.Company_Fax },
                Products = from m in data.Products
                           select new Products_Print() { Product_ID = m.Product_ID, Product_Name = m.Product_Name, Amount = m.Amount, Product_Detail = m.Product_Detail, Price = m.Price, Discount = m.Discount, Subtotal = m.Subtotal }
            });
            */

            return Ok(new { Status_Code = 2000, ID = data.Quotation.Quotation_ID });
        }


        private string GetDate()
        {
            string getdate;

            getdate = DateTime.Now.ToString("yyyyMM");

            var Quotations = from m in _context.Quotations
                             select m;
            if (!String.IsNullOrEmpty(getdate))
            {
                Quotations = Quotations.Where(s => s.Quotation_ID.Contains(getdate));
            }

            List<Quotation> l = Quotations.ToList();

            if (l.Count() == 0)
            {

            }

            int id_n = first(l);

            string id = Carry(id_n);


            getdate += id;
            return getdate;
        }
        private int first(List<Quotation> l)
        {
            if (l.Count() == 0)
            {
                return 1;
            }
            string s1 = l.OrderBy(x => x.Quotation_ID).Last().Quotation_ID;

            return Int32.Parse(s1.Substring(7, 2)) + 1;
        }
        private string Carry(int number)
        {
            if (number < 10)
            {
                return "_0" + number.ToString();
            }
            return "_" + number.ToString();
        }
        private bool QuotationExists(string id)
        {
            return _context.Quotations.Any(e => e.Quotation_ID == id);
        }
 
        [HttpGet("{id}")]
        public async Task<ActionResult<Search_Customer_Exist>> GetQuotation(string id)
        {



            var qs = _context.Set<Quotation>().ToDictionary(e => e.Quotation_ID)[id];

            string[] x = qs.Customer_ID.Split("_");

            var qss = _context.Set<Customer_Employee>().ToDictionary(e => e.ID)[qs.Customer_ID];

            var qssss = _context.Set<Customer_Company>().ToDictionary(e => e.Company_ID)[qss.Company_ID];

            var qsss = _context.Set<Quotation_Products>().ToDictionary(e => e.ID).Where(e=>e.Value.Quotation_ID == id);

            var qsssss = _context.Set<Payment>().ToDictionary(e => e.Payment_ID)[id];
           

            string[] subs = qs.Remark.Split('\n');

            return Ok(new
            {
                Status_Code = 2000,
                data = new Data_Print()
                {
                    Quotation = new Quotation_Print() { Project_Name = qs.Project_Name, Quotation_ID = qs.Quotation_ID, Date = qs.Date.ToString("yyyyMMdd"), Quotation_Deadline = qs.Quotation_Deadline.ToString("yyyyMMdd"), total = qs.Total, tax = qs.Total * 0.05m, total_withTax = qs.Total_withTax, Customer_ID = qs.Customer_ID, Remark = subs },
                Customer = new Customer_Print() { Customer_ID = qss.Company_ID, Customer_Name = qss.Customer_Name, Company_Name = qssss.Company_Name , Company_Phone = qss.Customer_Phone , Company_Fax=qssss.Company_Fax},
                    Products = from m in qsss
                               join ms in _context.Set<Product>()
                               on m.Value.Product_ID equals ms.Product_ID
                               select new Products_Print() { Product_ID = m.Value.Product_ID, Product_Name = ms.Product_Name, Amount = m.Value.Amount, Product_Detail = m.Value.Product_Detail, Price = m.Value.Price, Discount = m.Value.Discount, Subtotal = m.Value.Subtotal },
                    Payment = new Payment_Print () { Payment_Method=qsssss.Payment_Method}
                }
            });
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutQuotation(string id, Data data)
        {
            using var transaction = _context.Database.BeginTransaction();



            var q = _context.Quotations.Find(id);

          

            q.Total = data.Quotation.Total;

            q.Total_withTax = data.Quotation.Total * 1.05m;

            q.Project_Name = data.Quotation.Project_Name;

            q.Remark = data.Quotation.Remark;

            _context.Entry(q).State = EntityState.Modified;


            if (String.IsNullOrEmpty(data.Payment.Payment_Method))
            {
                data.Payment.Payment_Method = "一次付清";
            }


            var qqs = _context.Payments.Find(id);

            qqs.Payment_Method = data.Payment.Payment_Method;

            _context.Entry(qqs).State = EntityState.Modified;

            int x;
            // 設 x 值 為 填入 報價單_產品列表 的 產品編號
            // 若無此值 報價單_產品列表 的 產品為空 則 出錯
            // 先假設 x 為 產品 在之後做 報價單_產品列表 做 填寫時 才不會出錯
            if (_context.Set<Product>().Count() == 0)
            //產品若空的
            {
                x = 0;
                //設0
            }
            else
            {
                x = _context.Set<Product>().OrderBy(e => e.Product_ID).Last().Product_ID;
                //找最後一筆
            }

            for (int i = 0; i < data.Products.Count(); i++)
            //for 去跑 data.產品.長度
            {
                if (String.IsNullOrEmpty(data.Products[i].Product_Name) || String.IsNullOrEmpty(data.Products[i].Product_Detail) || data.Products[i].Price <= 0 || data.Products[i].Amount <= 0 || data.Products[i].Discount <= 0 || data.Products[i].Subtotal <= 0)
                //判斷值是否為 "" 或 null   0 或 負值
                //若是回傳錯誤
                {
                    return Ok(new { Status_Code = 4102, Message = "產品格式錯誤" });
                }


                if (data.Products[i].Product_ID == 0)
                //判斷產品是否為空
                //若是新增產品
                {
                    _context.Products.Add(new Product() { Product_Name = data.Products[i].Product_Name, Price = data.Products[i].Price, Product_Detail = data.Products[i].Product_Detail });
                    //重構 產品
                    //產品 Add
                    x++;
                    data.Products[i].Product_ID = x;
                    //假設 x 為 產品編號
                }

            }


            await _context.SaveChangesAsync();
            //Save 資料庫

            var qs = _context.Quotation_Products.Where(e => e.Quotation_ID == id).ToList();

            for(int o = 0; o < qs.Count(); o++)
            {
                _context.Remove(qs[o]);
            }

            for (int i = 0; i < data.Products.Count(); i++)
            {

                 _context.Quotation_Products.Add(new Quotation_Products() { Quotation_ID = q.Quotation_ID, Product_ID = data.Products[i].Product_ID, Amount = data.Products[i].Amount, Price = data.Products[i].Price, Discount = data.Products[i].Discount, Subtotal = data.Products[i].Subtotal, Product_Detail = data.Products[i].Product_Detail });

               
            //重構 報價單_產品列表
                //報價單_產品列表 Add
            }
            await _context.SaveChangesAsync();

            transaction.Commit();


            string[] subs = q.Remark.Split('\n');
            // 把 備註 String 截斷\n 成為 備註 Array

            /*
            var getpdf = _ipdfService.GeneratePdfReport(new Data_Print()
            // PDF 
            {
                Quotation = new Quotation_Print() { Project_Name = q.Project_Name, Quotation_ID = q.Quotation_ID, Date = q.Date.ToString("yyyyMMdd"), Quotation_Deadline = q.Quotation_Deadline.ToString("yyyyMMdd"), total = q.Total, total_withTax = q.Total_withTax, Customer_ID = q.Customer_ID, Remark = subs },
                Customer = new Customer_Print() { Customer_ID = data.Customer.Customer_ID, Customer_Name = data.Customer.Customer_Name, Company_Name = data.Customer.Company_Name, Company_Phone = data.Customer.Company_Phone, Company_Fax = data.Customer.Company_Fax },
                Products = from m in data.Products
                           select new Products_Print() { Product_ID = m.Product_ID, Product_Name = m.Product_Name, Amount = m.Amount, Product_Detail = m.Product_Detail, Price = m.Price, Discount = m.Discount, Subtotal = m.Subtotal }
            });

            return File(getpdf, "application/pdf", data.Quotation.Quotation_ID + ".pdf");
            */
            return Ok(new { Status_Code = 2000, ID = data.Quotation.Quotation_ID });
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customer>> DeleteQuotation(string id)
        {
            var query = _context.Quotations.Find(id);

            query.IsDelete = true;

            _context.Entry(query).State = EntityState.Modified;

            await _context.SaveChangesAsync();


            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }



    }
}
