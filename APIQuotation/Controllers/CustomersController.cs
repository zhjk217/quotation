﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIQuotation.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIQuotation.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly QuotationContext _context;

        public CustomersController(QuotationContext context)
        {
            _context = context;
        }

        // GET: api/Customers
        [HttpGet]
        public async Task<ActionResult> GetCustomers([FromQuery]string Search)
        {

            var Success_Message = new { Status_Code = 2000, Data = await List_AllCustomers(Search).ToListAsync() };
            return Ok(Success_Message);
        }
        [NonAction] 
        private IQueryable<Customer_List> List_AllCustomers(String Search)
        {
            var Customers = from m in _context.Set<Customer_Company>()
                            where m.IsDelete == false
                            select new Customer_List()
                            {
                                Company_ID=m.Company_ID,
                                Company_Name=m.Company_Name,
                                Company_Fax=m.Company_Fax,
                                Customer = from md in m.Customer_Employees
                                           where !md.IsDelete
                                           select new Customer_Employee_List (){ ID=md.ID, Customer_Name=md.Customer_Name, Customer_Phone=md.Customer_Phone }
                            };

            if (!string.IsNullOrEmpty(Search))
            {
                Customers = Customers.Where(e => e.Company_Name.Contains(Search));
            }

            return Customers;
        }
    }
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly QuotationContext _context;

        public CustomerController(QuotationContext context)
        {
            _context = context;
        }
        [Route("Company/{id}")]
        [HttpPut]
        public async Task<IActionResult> PutCustomer(string id, Customer_Company customer)
        {
            if (!_context.Customer_Companys.Any(e=>e.Company_ID ==id))
            {
                return Ok(new {Status_Code= 4103, Message = "編號不存在"});
            }
            if (String.IsNullOrEmpty(customer.Company_Name) || String.IsNullOrEmpty(customer.Company_Fax))
            {
                return Ok(new { Status_Code = 4102, Message = "客戶格式錯誤" });
            }

            _context.Entry(new Customer_Company() { Company_ID= id, Company_Name=customer.Company_Name,Company_Fax=customer.Company_Fax}).State = EntityState.Modified;

            await _context.SaveChangesAsync();
    
            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        [Route("Company/{id}")]
        [HttpDelete]
        public async Task<ActionResult<Customer>> DeleteCompany(string id)
        {
            if (!_context.Customer_Companys.Any(e => e.Company_ID == id))
            {
                return Ok(new { Status_Code = 4103, Message = "編號不存在" });
            }

            var query = _context.Customer_Companys.Find(id);

            query.IsDelete = true;

            _context.Entry(query).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        [Route("Employee/{id}")]
        [HttpPut]
        public async Task<IActionResult> PutEmployee(string id, Customer_Employee customer)
        {
            if (!_context.Customer_Employees.Any(e => e.ID == id))
            {
                return Ok(new { Status_Code = 4103, Message = "編號不存在" });
            }
            if (String.IsNullOrEmpty(customer.Customer_Name) || String.IsNullOrEmpty(customer.Customer_Phone))
            {
                return Ok(new { Status_Code = 4102, Message = "客戶格式錯誤" });
            }

            _context.Entry(new Customer_Employee() { ID = id, Company_ID = customer.Company_ID, Customer_Name = customer.Customer_Name, Customer_Phone = customer.Customer_Phone }).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        [Route("Employee/{id}")]
        [HttpDelete]
        public async Task<ActionResult<Customer>> DeleteEmployee(string id)
        {
            if (!_context.Customer_Employees.Any(e => e.ID == id))
            {
                return Ok(new { Status_Code = 4103, Message = "編號不存在" });
            }

            var query = _context.Customer_Employees.Find(id);

            query.IsDelete = true;

            _context.Entry(query).State = EntityState.Modified;

            await _context.SaveChangesAsync();

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        private bool CompanyExists(string id)
        {
            return _context.Customer_Companys.Any(e => e.Company_ID == id);
        }
        private bool CustomerExists(string id)
        {
            return _context.Customer_Employees.Any(e => e.Company_ID == id);
        }
    }
}
