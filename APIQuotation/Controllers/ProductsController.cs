﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIQuotation.Models;
using Microsoft.AspNetCore.Authorization;

namespace APIQuotation.Controllers
{
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly QuotationContext _context;

        public ProductsController(QuotationContext context)
        {
            _context = context;
        }

        // GET: api/Customers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> GetProducts([FromQuery]  Verify verify)
        {
            var Products = from m in _context.Products
                           where m.IsDelete == false
                           select new { m.Product_ID, m.Product_Name, m.Product_Detail,m.Price };

            var Success_Message = new { Status_Code = 2000, Data = await Products.ToListAsync() };
            return Ok(Success_Message);
        }

    }
    [Authorize]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly QuotationContext _context;

        public ProductController(QuotationContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> SearchProducts([FromQuery]  Verify_Product verify)
        {
            var Products = from m in _context.Products
                           where m.IsDelete == false
                           where m.Product_Name.Contains(verify.Search) || m.Price.ToString().Contains(verify.Search)
     
                           select new { m.Product_ID,m.Product_Name, m.Product_Detail,m.Price};

            var Success_Message = new { Status_Code = 2000 , Data = await Products.ToListAsync() };
            return Ok(Success_Message);
        }
        // POST: api/Customers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Product>> PostProduct(Data_Product data)
        {

            _context.Products.Add(data.Product);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductExists(data.Product.Product_ID))
                {
                    var Wrong_Message = new { Status_Code=4103, Message = "編號衝突" };
                    return Ok(Wrong_Message);
                }
                else
                {
                    throw;
                }
            }

            var Success_Message = new { Status_Code = 2000, Product_ID = data.Product.Product_ID };
            return Ok(Success_Message);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<Search_Customer_Exist>> GetProduct(int id, [FromQuery] Verify verify)
        {
            var Products = from m in _context.Products
            select new { m.Product_ID,m.Product_Name,m.Price };

            var Productss = _context.Products.Find(id);

            var Success_Message = new { Status_Code = 2000, Data = new {  Productss.Product_Name, Productss.Product_Detail,Productss.Price } };
            return Ok(Success_Message);
        }
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduct(int id, Product product)
        {
            product.Product_ID = id;

            _context.Entry(product).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ProductExists(id))
                {
                    var Wrong_Message = new { Status_Code = 4103, Message = "編號衝突" };
                    return Ok(Wrong_Message);
                }
                else
                {
                    throw;
                }
            }

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<Customer>> DeleteProduct(int id)
        {
            var query = _context.Products.Find(id);

            query.IsDelete = true;

            _context.Entry(query).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (ProductExists(id))
                {
                    var Wrong_Message = new { Status_Code = 4103, Message = "編號衝突" };
                    return Ok(Wrong_Message);
                }
                else
                {
                    throw;
                }
            }

            var Success_Message = new { Status_Code = 2000 };
            return Ok(Success_Message);
        }
        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Product_ID == id);
        }
    }
}
