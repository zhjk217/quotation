﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIQuotation.Models
{
    public class Quotation
    {
        [Key]
        // 報價單編號
        //[Column(TypeName = "char(9)")]
        //[Required]
        public string Quotation_ID { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        //[Required]
        public string Project_Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime Quotation_Deadline { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        //[Required]
        public string Project_Owner { get; set; }
        //[Column(TypeName = "varchar(100)")]
        public string Project_Signback { get; set; }
        //[Column(TypeName = "nvarchar(6)")]
        //[Required]
        public string Status { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        //[Required]
        public decimal Total { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        //[Required]
        public decimal Total_withTax { get; set; }
        public List<Quotation_Products> Quotation_Products { get; set; }
        //[Column(TypeName = "varchar(10)")]
        //[Required]
        public string Customer_ID { get; set; }
        public Customer_Employee Customer_Employee { get; set; }

        public Payment Payment { get; set; }
        //[Column(TypeName = "nvarchar(150)")]
        //[Required]
        public string Remark { get; set; }
        //[Column(TypeName = "nvarchar(50)")]
        //[Required]
        public string Project_Remark { get; set; }
        //[Required]
        public bool IsSignback { get; set; }
        //[Required]
        public bool IsDelete { get; set; }
        //[Column(TypeName = "char(9)")]
        //[Required]
        //public string Company_ID { get; set; }
        //public Office Office { get; set; }
    }
    public class Quotation_List
    {

        public string Quotation_ID { get; set; }
        public string Company_Name { get; set; }
        public string Project_Name { get; set; }
        public bool IsSignback { get; set; }
    }
    public class StatusQuotation_List
    {
        public string Status { get; set; }
        public string Quotation_ID { get; set; }
        public string Project_Name { get; set; }
        public string Payment_Status { get; set; }
        public string Payment_Method { get; set; }
        public string Project_Remark { get; set; }
    }
    public class Product
    {
        [Key]
        public int Product_ID { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Product_Name { get; set; }
        //[Column(TypeName = "nvarchar(50)")]
        public string Product_Detail { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        public bool IsDelete { get; set; }

        public List<Quotation_Products> Quotation_Products { get; set; }
    }
    public class Quotation_Products
    {
        [Key]
        public int ID { get; set; }
        //[Column(TypeName = "char(9)")]
        public string Quotation_ID { get; set; }
        public Quotation Quotation { get; set; }


        public int Product_ID { get; set; }
        public Product Product { get; set; }
        //[Column(TypeName = "nvarchar(100)")]
        public string Product_Detail { get; set; }
        public int Amount { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        public decimal Discount { get; set; }
        //[Column(TypeName = "decimal(18, 2)")]
        public decimal Subtotal { get; set; }
        public bool IsDelete { get; set; }
    }
    public class Customer
    {
        [Key]

        public string Customer_ID { get; set; }

        public string Customer_Name { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Company_Name { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Company_Fax { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Company_Phone { get; set; }
        public bool IsDelete { get; set; }
        public List<Quotation> Quotations { get; set; }
    }
    public class Customer_Company
    {
        [Key]
        //[Column(TypeName = "char(9)")]
        public string Company_ID { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Company_Name { get; set; }
        //[Column(TypeName = "nvarchar(30)")]
        public string Company_Fax { get; set; }
        public bool IsDelete { get; set; }
        public List<Customer_Employee> Customer_Employees { get; set; }
    }
    public class Customer_Employee
    {
        [Key]
        public string ID { get; set; }
        public string Company_ID { get; set; }
        public Customer_Company Customer_Company { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Phone { get; set; }
        public bool IsDelete { get; set; }

        public List<Quotation> Quotations { get; set; }
    }
    public class Customer_List
    {
        public string Company_ID { get; set; }
        public string Company_Name { get; set; }
        public string Company_Fax { get; set; }
        public IEnumerable<Customer_Employee_List> Customer { get; set; }
    }
    public class Customer_Employee_List
    {
        public string ID { get; set; }
        public string Customer_Name { get; set; }
        public string Customer_Phone { get; set; }
    }
    public class Payment
    {
        [Key]
        public string Payment_ID { get; set; }
        public string Payment_Method { get; set; }
        public string Payment_Status { get; set; }
        public decimal Total { get; set; }

        public int Invoice_Month { get; set; }
        public string Invoice_Number { get; set; }

        public Quotation Quotation { get; set; }
        public bool IsDelete { get; set; }


    }

    public class Item_Get
    {
       public string Quotation_ID { get; set; }
       public string Company_Name { get; set; }
       public List<string> Products { get; set; }
    }
    public class Item_Product
    {
        public Product Product { get; set; }
        public Quotation_Products Quotation_Products { get; set; }
        
    }

    public class Search_Customer_Exist
    {
        public Customer Customer { get; set; }
        public bool Customer_Exist { get; set; }



    }

}
