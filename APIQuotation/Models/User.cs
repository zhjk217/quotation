﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace APIQuotation.Models
{
    public class User
    {
        [Key]
        public int User_ID { get; set; }
        public string Account { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Company_ID { get; set; }
        public Office Company { get; set; }
    }
    public class User_Signup
    {
        public string Account { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Company_ID { get; set; }
    }
    public class User_Signin
    {
        public string Account { get; set; }
        public string Password { get; set; }
    }
    public class Office
    {
        [Key]
        public string Company_ID { get; set; }
        public string Company_Name { get; set; }
        public string Company_Address { get; set; }
        public string Company_Phone { get; set; }
        public string Company_Email { get; set; }

        public List<User> Users { get; set; }
        public List<Project> Projects { get; set; }
        //public List<Quotation> Quotations { get; set; }
    }
    public class Project
    {
        [Key]
        public int ID { get; set; }
        public string Company_ID { get; set; }
        public string Project_Name { get; set; }
        public bool IsDelete { get; set; }
        public Office Company { get; set; }
    }
    public class Verify
    {
        public int user_id { get; set; }
        public string token { get; set; }
    }
    public class Data
    {
        public Customer Customer { get; set; }
        public List<QPs> Products { get; set; }
        public Quotation Quotation { get; set; }
        public Payment Payment { get; set; }
    }
    public class QPs
    {
        public int Product_ID { get; set; }
        public string Quotation_ID { get; set; }
        public string Product_Name { get; set; }
        public string Product_Detail { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public decimal Subtotal { get; set; }
        public bool Isdelete { get; set; }
    }
    public class Success_Message_PostQuotation
    {
        public int Status_Code { get; set; }
        public string Quotation_ID { get; set; }
    }
    public class Success_Message_GetAllCustomers
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Customer> Data { get; set; }
    }
    public class Success_Message_SearchCustomer
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Customer> Data { get; set; }
    }
    public class Success_Message_GetAllProducts
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Product> Data { get; set; }
    }
    public class Success_Message_SearchProduct
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public List<Product> Data { get; set; }
    }
    public class Success_Message_PostCustomer
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Customer_ID { get; set; }
    }
    public class Data_Customer
    {
        public int User_ID { get; set; }
        public string token { get; set; }
        public Customer Customer { get; set; }
    }
    public class Verify_Customer
    {
        public int User_ID { get; set; }
        public string token { get; set; }
        public int Search_In { get; set; }
        public string Search { get; set; }
    }
    public class Data_Product
    {
        public int User_ID { get; set; }
        public string token { get; set; }
        public Product Product { get; set; }
    }
    public class Verify_Product
    {
        public int User_ID { get; set; }
        public string token { get; set; }
        public string Search { get; set; }
    }
    public class Success_Message_PostProduct
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public int Product_ID { get; set; }
    }
    public class Success_Message_GetAccount
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string Account { get; set; }
    }
    public class Quotation_p
    {
        public int Signback { get; set; }
        public int Year { get; set; }
        public int  Month { get; set; }
        public string Search { get; set; }
    }
    public class year_front
    {
        public string text { get; set; }
        public int value { get; set; }
    }
    public class Quotation_ps
    {
        public int Signback { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
        public string Search { get; set; }
    }

    public class Quotation_Status
    {
        public int Status { get; set; }
        public int Payment_Method { get; set; }
        public int Payment_Status { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string Search { get; set; }
    }

    public class Verify_Quotation_PUT_Status
    {
        public int Status { get; set; }
        public int Payment_Method { get; set; }
        public int Payment_Status { get; set; }
        public string Remark { get; set; }
    }
    public class Verify_Pdf
    {
        public int user_id { get; set; }
        public string token { get; set; }
        public string Quotation_ID { get; set; }
    }
    public class vs
    {
        public string Quotation_ID { get; set; }
        public string Company_Name { get; set; }
        public string[] Products { get; set; }
    }
    public class Quotation_Print
    {
        public string Quotation_ID { get; set; }
        public string Project_Name { get; set; }
        public string Date { get; set; }
        public string Quotation_Deadline { get; set; }
        public decimal total { get; set; }
        public decimal tax { get; set; }
        public decimal total_withTax { get; set; }
        public string Customer_ID { get; set; }
        public string[] Remark { get; set; }
    }
    public class Customer_Print
    {
        public string Customer_ID { get; set; }
        public string Customer_Name { get; set; }
        public string Company_Name { get; set; }
        public string Company_Fax { get; set; }
        public string Company_Phone { get; set; }
    }
    public class Products_Print
    {
        public int Product_ID { get; set; }
        public string Product_Name { get; set; }
        public string Product_Detail { get; set; }
        public int Amount { get; set; }
        public decimal Price { get; set; }
        public decimal Discount { get; set; }
        public decimal Subtotal { get; set; }
    }
    public class Data_Print
    {
        public Quotation_Print Quotation { get; set; }
        public Customer_Print Customer { get; set; }
        public IEnumerable<Products_Print> Products { get; set; }
        public Payment_Print Payment { get; set; }
    }
    public class Payment_Print
    {
        public string Payment_Method { get; set; }
    }
    public class Verify_File
    {
        public int user_id { get; set; }
        public string token { get; set; }
        public IFormFile File { get; set; }
    }
}
