﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using APIQuotation.Models;

namespace APIQuotation.Models
{
    public class QuotationContext : DbContext
    {
        public QuotationContext(DbContextOptions<QuotationContext> options)
        :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Quotation>()
                .HasOne(e => e.Customer_Employee)
                .WithMany(e => e.Quotations)
                .HasForeignKey(p => p.Customer_ID);

            modelBuilder.Entity<Quotation_Products>()
            .HasKey(t => new { t.ID });

            modelBuilder.Entity<Quotation_Products>()
            .HasOne(pt => pt.Quotation)
            .WithMany(p => p.Quotation_Products)
            .HasForeignKey(pt => pt.Quotation_ID);

            modelBuilder.Entity<Quotation_Products>()
            .HasOne(pt => pt.Product)
            .WithMany(t => t.Quotation_Products)
            .HasForeignKey(pt => pt.Product_ID);

            modelBuilder.Entity<Payment>()
                .HasOne(e => e.Quotation)
                .WithOne(e => e.Payment)
                .HasForeignKey<Payment>(p => p.Payment_ID);

            modelBuilder.Entity<User>()
                .HasOne(e => e.Company)
                .WithMany(e => e.Users)
                .HasForeignKey(p => p.Company_ID);


            modelBuilder.Entity<Customer_Employee>()
                .HasOne(e => e.Customer_Company)
                .WithMany(e => e.Customer_Employees)
                .HasForeignKey(p => p.Company_ID);

            modelBuilder.Entity<User>()
                .HasAlternateKey(c => c.Account);
            modelBuilder.Entity<Project>()
                .HasOne(e => e.Company)
                .WithMany(e => e.Projects)
                .HasForeignKey(p => p.Company_ID);
        }

        public DbSet<Quotation> Quotations { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Customer_Company> Customer_Companys { get; set; }
        public DbSet<Customer_Employee> Customer_Employees { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Quotation_Products> Quotation_Products { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Office> Office { get; set; }
    }
}
