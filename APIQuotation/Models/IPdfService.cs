﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Wkhtmltopdf.NetCore;

namespace APIQuotation.Models
{
    public interface IPdfService
    {
        public byte[] GeneratePdfReport(Data_Print data);
    }

    public class PdfService : IPdfService
    {
        private readonly string _folder;
        readonly IGeneratePdf _generatePdf;

        public PdfService(IWebHostEnvironment environment, IGeneratePdf generatePdf)

        {
            _folder = $@"{environment.WebRootPath}";
            _generatePdf = generatePdf;
        }
        public byte[] GeneratePdfReport(Data_Print data)
        {
            var sb = new StringBuilder();
            var path = $@"{_folder}";
            sb.Append(@"<html>
<head>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <title>Main</title>");
            sb.Append(@"<link rel='stylesheet' href='https://localhost:44312/bootstrap.min.css' rel='stylesheet' type='text/css'/> ");
            sb.Append(@"<style>
#border1{
  border-top:solid; 
  border-left:solid; 
  border-bottom:solid;
}
#border2{
  border-left:solid; 
  border-bottom:solid;
}
#border3{
  border-right:solid;
  border-left:solid; 
  border-bottom:solid;
}</style>");
            sb.Append(@"
</head>
<body>
  <div id='bg'>
    <div class='container'>
      <div class='row'>
         <div class='col-2'>
           <img v-bind='mainProps' src='https://localhost:44312/WM_logo.png' alt='img1' >
         </div>
         <div class='col'>
           <h2><b>麥威科技股份有限公司</b></h2>
         </div>
         <div class='col-3' style='color:Gray'>
           <h2 align='center' style='color:grey'><b>報價單</b></h2>
           <h5 align='center' style='color:grey'><b>Quotation</b></h5>
           <div class='bg-dark'><hr size=5></div>
         </div>
        </div>
      <div class='row'>
         <div class='col-5  offset-2'>
           <p>桃園市中壢區明德路60五樓I室</p>
           <p>電&emsp;&emsp;話：0918988747</p>
           <p>統一編號：82782328</p>
         </div>
         <div class='col'>
           <p>&emsp;</p>
           <p>&emsp;</p>
           <p align='right'>Email：mywaytech@getmywaytech.com</p>
         </div>
        </div>
      <div class='bg-dark'><hr size=7></div>
      <div class='row'>
         <div class='col'>");
            sb.Append(@"<p><b>客戶姓名：" + data.Customer.Customer_Name + "</b></p>");
            sb.Append(@"<p><b>公司名稱：" + data.Customer.Company_Name + "</b></p>");
            sb.Append(@"<p><b>公司電話：" + data.Customer.Company_Phone + "</b></p>");
            sb.Append(@"<p><b>公司傳真：" + data.Customer.Company_Fax + "</b></p>");
            sb.Append(@"
         </div>
        <div class='col'>
            <div class='row' style='position:fixed;right:0px'>
        <div class='col' style='text-align:right;width:200px' >
  
           <p><b>日期：</b></p>
           <p><b>報價單編號：</b></p>
           <p><b>客戶統一編號：</b></p>
           <p><b>報價有效期至：</b></p>
          </div>
         <div class='col' style='text-align:left;width:130px'>");
            sb.Append(@"<p><b>" + data.Quotation.Date + "</b></p>");
            sb.Append(@"<p><b>" + data.Quotation.Quotation_ID + "</b></p>");
            sb.Append(@"<p><b>" + data.Customer.Customer_ID + "</b></p>");
            sb.Append(@"<p><b>" + data.Quotation.Quotation_Deadline + "</b></p>");
            sb.Append(@"
         </div>
      </div>
    </div>
</div>
      <div class='bg-dark'><hr size=7></div>
       <p><b>特別注意事項：</b>1.本報價單經雙方協議成交，經客戶簽章視為正式訂單。</p>
       <div class='flieds'>
         <div class='row'>
           <div class='col' id='border1'>
             <p align='center'><b>品名編號</b></p>
           </div>
           <div class='col-4' id='border1'>
             <p align='center'><b>產品規格</b></p>
           </div>
           <div class='col' id='border1'>
             <p align='center'><b>數量</b></p>
            </div>
           <div class='col-2' id='border1'>
             <p align='center'><b>單價</b></p>
            </div>
           <div class='col-2' id='border1'>
             <p align='center'><b>特價(未稅)</b></p>
            </div>
           <div class='col-2' style='border-style:solid'>
             <p align='center'><b>小計</b></p>
            </div>
          </div>
       </div>
       <div>");
            var query = data.Products.ToList();
            for (int i = 0; i < data.Products.Count(); i++)
            {
                var col1 = $"<div class='col' id='border2'><p align='center'>" + (i + 1) + "</p></div>";

                var col2 = $"<div class='col-4' id='border2'><p>" + query[i].Product_Name + "<br>" + query[i].Product_Detail + "</p></div>";

                var col3 = $"<div class='col' id='border2'><p align='center'>" + query[i].Amount + "</p></div>";

                var col4 = $"<div class='col-2' id='border2'><p align='right'>NT$" +decimal.ToInt32(query[i].Price) + "</p></div>";

                var col5 = $"<div class='col-2' id='border2'><p align='right'>NT$" +decimal.ToInt32(query[i].Discount) + " </p></div>";

                var col6 = $"<div class='col-2'id='border3'><p align='right'>NT$" + decimal.ToInt32(query[i].Subtotal) + "</p></div>";

                var row = $"<div class='row'>" + col1 + col2 + col3 + col4 + col5 + col6 + "</div>";

                sb.Append(@row);
            }


            sb.Append(@"
       <div>
         <div class='row'>
           <div class='col-6'>
             <p><b>備註：<br>");
            for (int i = 0; i < data.Quotation.Remark.Count(); i++)
            {
                sb.Append(i + 1 + "." + data.Quotation.Remark[i] + "<br>");
            }
            sb.Append(@"
            </b></p></div>
           <div class='col'>
             <div class='row'>
               <div class='col'></div>
                <div class='col-4' id='border2'>
                 <p align='right' >小計</p>
                </div>
               <div class='col-4' id='border3'>
                 <p align='right'>NT$");
            sb.Append(decimal.ToInt32(data.Quotation.total));
            sb.Append(@" </p>
               </div>
            </div>
             <div class='row'>
                <div class='col'></div>
                <div class='col-4' id='border2'>
                 <p align='right'>稅額</p>
                </div>
               <div class='col-4' id='border3'>
                 <p align='right'>NT$");
            decimal x = 0.05m;
            sb.Append(decimal.ToInt32(data.Quotation.total * x));
            sb.Append(@" </p>
               </div>
            </div>
             <div class='row'>
                <div class='col'></div>
                <div class='col-4' id='border2'>
                 <p align='right'><b>總計</b></p>
                </div>
               <div class='col-4' id='border3'>
                 <p align='right'>NT$");
            decimal y = 1.05m;
            sb.Append(decimal.ToInt32(data.Quotation.total_withTax));
            sb.Append(@" </ p >
               </div>
            </div>
        </div>
          
    </div>
       </div>
       <div class='row'>

        </div>
      <br>
      <div class='row'>
         
    </div>
    <div class='row'>
        <div class='col-4'>
         <p align='center' style='border-style:solid'><b>客戶同意採購簽章：</b></p>
         <br><br>
        </div>
    </div>
      <div class='row'>
        <div class='col-6'>
         <div class='bg-dark'><hr></div>
        </div>
    </div>
      <div class='row'>
        <div class='col'>
         <div class='bg-dark'><hr size=5></div>
         <p align='center'>如您有任何疑問，請即聯絡：薛勝榮，0918988747，austin@getmywaytech.com</p>
         <p align='center'><b>祝事業興旺！</b></p>
        </div>
    </div>
    </div>

  </div>
</body>
</html>

        "); 
            return _generatePdf.GetPDF(sb.ToString());
        }
    }
}
